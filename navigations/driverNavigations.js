import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from "../screens/homeScreen";
import LoginScreen from "../screens/loginScreen";
import NewOrder from '../screens/newOrder';
//helloo
const Stack = createNativeStackNavigator();

export default function MyStack() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                height: '100%'
            }}
        >
            <Stack.Screen name="NewOrder" component={NewOrder} />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Home" component={HomeScreen} />

        </Stack.Navigator>
    );
}

// option={
//     {
//         headerStatusBarHeight: 35,
//         headerBackground: () => (
//             <Image source={logo}
//                 style={{ width: '100%', height: '90%', alignSelf: 'center' }} />
//         ),
//         title: ''
//     }}