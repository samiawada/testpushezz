import React, { useEffect } from 'react'
import { Text, View, StyleSheet, Animated, Easing, TouchableOpacity, Dimensions } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import MapView, { Marker } from 'react-native-maps';
import SwipeButton from 'rn-swipe-button';
import Icon from 'react-native-vector-icons/Fontisto';
import { Audio } from 'expo-av';
import { LinearGradient } from 'expo-linear-gradient';
//helloo
const boxWidth = Dimensions.get('window').width - 40

const styles = StyleSheet.create({
    container: {
        height: '100%',
    },
    text: {
        color: 'white',
        fontSize: 25,
        fontWeight: 'bold'
    },
    button: {
        elevation: 9,
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 20
    },
    buttonText: {
        fontSize: 18,

    }
})

const AnimatedCircle = Animated.createAnimatedComponent(AnimatedCircularProgress)

export default function NewOrder({ navigation }) {
    useEffect(async () => {
        Audio.setAudioModeAsync({
            allowsRecordingIOS: false,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            playsInSilentModeIOS: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DUCK_OTHERS,
            shouldDuckAndroid: true,
            staysActiveInBackground: true,
            playThroughEarpieceAndroid: true
        })
        const { sound } = await Audio.Sound.createAsync(
            require('../playSound/warning.mp3')
        );

        await sound.playAsync();
    }, [])

    return (


        <LinearGradient colors={['#000d12', '#000b0f']} style={styles.container} >
            <View style={{ alignItems: 'flex-end' }}>
                <TouchableOpacity
                    style={{
                        width: 100,
                        borderWidth: 1.5,
                        borderColor: 'red',
                        borderRadius: 10,
                        marginTop: '10%',
                        marginRight: 25,
                        display: 'flex',
                        flexDirection: 'row',
                        backgroundColor: 'none',
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: 5
                    }}
                    onPress={() => navigation.navigate('Home')}
                >
                    <Icon
                        name='close-a'
                        size={10}
                        color='red'
                        style={{ position: 'absolute', left: 8, top: '47%' }}
                    />
                    <Text style={{
                        color: 'red',
                        fontWeight: 'bold'
                    }}>
                        Deny
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={{ top: '5%', alignItems: 'center', width: '100%' }}>
                <AnimatedCircle
                    size={250}
                    backgroundWidth={8}
                    width={8}
                    rotation={0}
                    prefill={0}
                    fill={100}
                    duration={22000}
                    easing={Easing.linear}
                    lineCap={'round'}
                    tintColor="#0011d1"
                    backgroundColor="#3d5875"
                >
                    {
                        () => (
                            <View style={{ height: '100%', width: '100%' }}>
                                <MapView
                                    style={{ height: '100%', width: '100%' }}
                                    showsUserLocation={false}
                                    zoomEnabled={true}
                                    zoomControlEnabled={true}
                                    initialRegion={{
                                        latitude: 28.579660,
                                        longitude: 77.321110,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                    }} >
                                    <Marker
                                        coordinate={{ latitude: 28.579660, longitude: 77.321110 }}
                                        title={"JavaTpoint"}
                                        colo
                                        description={"Java Training Institute"}
                                    />
                                </MapView>
                            </View>

                        )
                    }
                </AnimatedCircle>
                <View style={{ marginTop: 10 }}>
                    <Text style={{ color: 'white', fontSize: 26, fontWeight: 'bold' }}>New Order</Text>
                </View>
                <View style={{
                    width: boxWidth,
                    height: '30%',
                    backgroundColor: 'rgba(100, 117, 163, 0.3)',
                    borderWidth: 3,
                    borderColor: 'rgba(100, 117, 163, 0.3)',
                    borderRadius: 30,
                    justifyContent: 'flex-start',
                    marginTop: 15
                }}>
                    <Text style={{ color: 'white', margin: 7, fontSize: 22, fontWeight: 'bold' }}>MacDonald</Text>
                    <Text style={{ color: 'white', margin: 5, paddingLeft: 5, fontSize: 16 }}>From Tayyouneh,Badaro</Text>
                    <Text style={{ color: 'white', margin: 5, paddingLeft: 5, fontSize: 16 }}>To Chiyah,Badaro</Text>
                </View>
            </View>
            <View style={{ position: 'absolute', bottom: 0, width: '100%', margin: 0 }} >
                <SwipeButton
                    containerStyles={{ borderWidth: 0, borderRadius: 0, margin: 0, padding: 0 }}
                    height={50}
                    onSwipeSuccess={() => navigation.navigate('Home')}
                    railBackgroundColor="#0cf500"
                    railFillBackgroundColor='none'
                    railFillBorderColor='none'
                    railStyles={{ borderRadius: 10 }}
                    titleColor='white'
                    titleFontSize={22}
                    titleStyles={{ fontWeight: 'bold' }}
                    title="Slide To Accept Order"
                    thumbIconBackgroundColor='none'
                    thumbIconStyles={{ borderWidth: 0 }}
                    thumbIconWidth={50}
                    thumbIconComponent={() =>
                        <Icon
                            size={25}
                            name='angle-dobule-right'
                            color='white'
                        />}
                >
                </SwipeButton>
            </View>
        </LinearGradient>
    )
}
