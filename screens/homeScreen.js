import React, { useState, useRef } from 'react';
import { Text, View, DrawerLayoutAndroid, FlatList, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Header, Switch } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import logo from "../assets/alporto-2-02.png"
import { moderateScale } from 'react-native-size-matters';
//helloo
const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad33abb27ba',
        title: 'First Item',
    },
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3aw53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29f72',
        title: 'Third Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d42',
        title: 'Third Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d32',
        title: 'Third Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d12',
        title: 'Third Item',
    },
];

const Item = ({ title }) => (
    <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
    </View>
);

export default function Home({ navigation }) {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);

    const drawer = useRef(null);

    const navigationView = () => (
        <View style={[styles.container, styles.navigationContainer]}>
            <Text style={styles.paragraph}>I'm in the Drawer!</Text>
            <TouchableOpacity
                title="Close drawer"
                onPress={() => drawer.current.closeDrawer()}
            >
                <Text>Hello</Text>
            </TouchableOpacity>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item title={item.title} />
    );


    return (
        <DrawerLayoutAndroid
            ref={drawer}
            drawerWidth={300}
            drawerPosition={'left'}
            renderNavigationView={navigationView}
        >
            <View style={{ color: 'white', backgroundColor: 'blacks' }}>
                <Header
                    placement="left"
                    backgroundColor='black'
                    containerStyle={{
                        height: 60,
                        width: '100%',
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}
                >
                    <View style={styles.barView}>
                        <TouchableOpacity
                            onPress={() => drawer.current.openDrawer(drawer)}
                        >
                            <Icon
                                name='bars'
                                size={18}
                                color='white'
                            />
                        </TouchableOpacity>
                    </View>
                    <Image
                        source={logo}
                        style={styles.logo}
                    />
                    <Switch
                        trackColor={{ false: "grey", true: "gold" }}
                        thumbColor="white"
                        ios_backgroundColor="#3e3e3e"
                        onValueChange={toggleSwitch}
                        value={isEnabled}
                        style={styles.switchView}
                    />
                </Header>
            </View>
            <View style={styles.body}>
                <FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={item => item.id}
                />
            </View>
        </DrawerLayoutAndroid>
    );
}

const styles = StyleSheet.create({
    barView: {
        height: 35,
        marginVertical: 'auto',
        alignSelf: 'flex-start',
    },
    logo: {
        width: 170,
        height: 35,
        marginVertical: 'auto',
        alignSelf: 'center'
    },
    switchView: {
        width: 30,
        transform: [{ scaleX: moderateScale(0.8, 10) }, { scaleY: moderateScale(0.8, 2) }]
    },
    body: {
        backgroundColor: 'white',
        height: '100%',
        borderRightWidth: 3,
        borderLeftWidth: 3,
    },
    item: {
        backgroundColor: '#eba905',
        height: 110,
        borderBottomWidth: 1,
        borderEndWidth: 1,
        borderBottomColor: 'black',
        borderBottomRightRadius: 30,
        borderTopRightRadius: 15,
        marginTop: 1,
        marginRight: 3,
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
    },
})