import React from "react";
import logo from '../assets/alporto-1-02.png'
import { StyleSheet, View, TouchableOpacity, Text, Image } from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
//hello0
const appId = "1047121222092614"

export default function LoginScreen({ navigation }) {
    const [firstName, setFirstName] = React.useState("");
    const [lastName, setLastName] = React.useState("");
    const [password, setPassword] = React.useState("");

    return (
        <View style={styles.column}>
            <Image
                source={logo}
                style={styles.logo}
            />
            <View>
                <View style={styles.row}>
                    <View style={styles.inputWrap}>
                        <Input
                            style={styles.TextInput}
                            onChangeText={firstName => setFirstName(firstName)}
                            placeholder='First Name'
                            leftIcon={
                                <Icon
                                    style={{ marginRight: 10 }}
                                    name='user'
                                    size={18}
                                    color='white'
                                />
                            }
                        />
                    </View>

                    <View style={styles.inputWrap}>
                        <Input
                            style={styles.TextInput}
                            onChangeText={lastName => setLastName(lastName)}
                            placeholder='Last Name'
                            leftIcon={<Icon />}
                        />
                    </View>
                </View>
            </View>

            <View style={styles.passwordView}>
                <Input
                    style={styles.TextInput}
                    placeholder="Password"
                    placeholderColor="white"
                    onChangeText={password => setPassword(password)}
                    secureTextEntry={true}
                    leftIcon={
                        <Icon
                            style={{ marginRight: 10 }}
                            name='lock'
                            size={18}
                            color='white'
                        />
                    }
                />
            </View>

            <View style={{ marginTop: 2 }}>
                <TouchableOpacity
                    style={styles.loginButton}
                    onPress={() => navigation.navigate('Home')}
                >
                    <Text style={styles.text}>Login</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

}

const styles = StyleSheet.create({
    image: {
        backgroundColor: 'black',
        borderWidth: 2,
        borderColor: 'white',
        width: '100%',
        height: 105,
        marginHorizontal: 'auto',
    },
    logo: {
        width: '100%',
        height: 120,
        backgroundColor: 'black',
        borderRadius: 800 / 2,
        marginHorizontal: 'auto',
        marginTop: '30%',
        marginBottom: 30,
    },
    column: {
        backgroundColor: 'black',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
    },
    row: {
        flexDirection: "row",
        alignItems: 'center',
        alignSelf: 'center',
    },
    inputWrap: {
        width: '50%'
    },
    TextInput: {
        height: 43,
        fontSize: 18,
        color: 'white',
        width: '100%',
    },
    text: {
        fontSize: 16,
        width: '100%',
        textShadowColor: 'white',
        textShadowOffset: { width: 2, height: 2 },
        textShadowRadius: 4,
        fontWeight: "bold",
        textAlign: 'center'
    },
    passwordView: {
        width: '100%'
    },
    loginButton: {
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: 'gold',
        borderRadius: 80 / 2,
        marginTop: 20,
        width: 200,
        height: 30,
    },
})