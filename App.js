import { SafeAreaView } from 'react-native-safe-area-context';
import React from 'react';
import { StatusBar, View, StyleSheet } from 'react-native';
import MyStack from './navigations/driverNavigations';
import { NavigationContainer } from '@react-navigation/native';
//helloo  


export default function App() {
  return (
    <NavigationContainer>
      <View style={styles.status}>
        <SafeAreaView>
          <StatusBar hidden={true} disable={true} />
        </SafeAreaView>
      </View>
      <MyStack />
    </NavigationContainer>
  );
}

const Status_height = StatusBar.currentHeight;

const styles = StyleSheet.create({
  status: {
    height: 0,
    backgroundColor: 'white'
  }
})